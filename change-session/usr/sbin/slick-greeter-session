#!/bin/sh
# -------
# File:        slick-greeter-session
# Description: Workaround to change default slick-greeter session
# Author:      Luis Antonio Garcia Gisbert <luisgg@gmail.com> 
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along 
# with this program; if not, write to the Free Software Foundation, Inc., 
# 51 Franklin St, Fifth Floor, Boston MA 02110-1301 USA
# --------

# funcs
# -----

die(){
        echo "$1" >&2
        exit 1
}

usage(){
       die "Usage: $(basename "$0") {session_name|DEFAULT}"
}

[ "$1" ] || usage

XSESSIONS_DIR="/usr/share/xsessions"
CINNAMON="$XSESSIONS_DIR/cinnamon.desktop"
NEW_CINNAMON="$XSESSIONS_DIR/cinnamon-full.desktop"
DEFAULT="/var/lib/slick-greeter-change-session/default"

SESSION="$1"
if [ "$SESSION" = "DEFAULT" ] || [ "$SESSION" = "default" ] ; then
        if [ -r "$DEFAULT" ] ; then
        	SESSION="$(cat "$DEFAULT")"
	else
        	SESSION="cinnamon"
	fi
fi
if [ "$SESSION" = "cinnamon" ] ; then
        SESSION="cinnamon-full"
fi

if [ -z "$SESSION" ] ||	[ ! -e "${XSESSIONS_DIR}/${SESSION}.desktop" ] ; then
	die "Inexistent session"
fi

echo "$SESSION" > $DEFAULT
rm -f "$CINNAMON"
ln -s "${SESSION}.desktop" "$CINNAMON"
exit 0

